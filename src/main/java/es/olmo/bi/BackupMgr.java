package es.olmo.bi;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.schlichtherle.truezip.zip.ZipEntry;
import de.schlichtherle.truezip.zip.ZipFile;

public class BackupMgr {
	File root=null;
	protected Stat statDes = null;
	public BackupMgr(String rDest) throws Exception{
		root=new File(rDest);
		if(!root.exists() || !root.isDirectory())
			throw new RuntimeException("Root Destination does not exists:"+rDest);
	}
	
	public void init(String pthZIPs) throws Exception{
		File f=new File(pthZIPs);
		String lastpart=f.getName();
		statDes = new Stat(root.getCanonicalPath() ,lastpart);
	}

	public File getRootFile(){
		return root;
	}
	
	public String normalizeDir(String dirIn) throws IOException{
		if(!dirIn.startsWith("/") && !dirIn.substring(1,1).equals(":")){
			File local=new File(new File("."),dirIn);
			dirIn=local.getCanonicalPath();
		}
		StringBuffer out=new StringBuffer();
		StringTokenizer st=new StringTokenizer(dirIn,"/\\:");
		while(st.hasMoreElements()){
			if(out.length()>0)
				out.append("/");
			out.append(st.nextToken());
		}
		return out.toString();
	}
	
	Pattern pat=null;
	public void initMatcher(String rawPattern){
		String pattern=rawPattern.replace(".", "\\.");
		pattern=pattern.replace("?", ".");
		pattern=pattern.replace("*", ".*");
		pattern+=".*";
		pat=Pattern.compile(pattern);
	}
	public boolean match(String file){
		Matcher m=pat.matcher(file);
		return m.matches();
	}
	
	ZipFile openedZIP=null;
	String nameOfOpenedZIP="nonsesnseZipName";
	public ZipEntry getZEForName(String name, String zipname){
		try {
			if(!nameOfOpenedZIP.equals(zipname)){
				if(openedZIP!=null)
					openedZIP.close();
				openedZIP=null;
			}
			if(openedZIP==null){
				openedZIP=new ZipFile(new File(statDes.statPath,zipname));
				nameOfOpenedZIP=zipname;
			}
			return openedZIP.getEntry(name);
		}catch(Exception e){

		}
		return null;
	}
	public InputStream getISForFile(String name, String zipname){
		try {
			return openedZIP.getInputStream(name);
		}catch(Exception e){

		}
		return null;
	}

	byte buffer[] = new byte[8 * 1024 * 1024];

	public void flushFrom(InputStream is, OutputStream os, ProgressShower ps, String msg) throws IOException {
		int readed;
		while ((readed = is.read(buffer)) >= 0){
			os.write(buffer, 0, readed);
			if(ps!=null)
				ps.report(readed, msg);
		}
	}

}
