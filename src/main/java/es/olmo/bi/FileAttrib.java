package es.olmo.bi;

import java.io.File;
import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.Vector;

public class FileAttrib implements Serializable,Comparable<FileAttrib>{
	private static final long serialVersionUID = -16349874608525925L;
	//Father of this
	FileAttrib father=null;
	//Sons of this
	Vector<FileAttrib> sons=new Vector<>();
	//History of this
	Vector<String> zips=new Vector<>();
	Vector<Date> fechas=new Vector<>();
	//File attributes (last version)
	String name;
	long size;
	long time;
	boolean deleted;
	
	public FileAttrib(String n, long s, long t){
		name=n;
		size=s;
		time=t;
		deleted=false;
	}
	
	public void addHistory(Date d, String zip){
		fechas.add(d);
		zips.add(zip);
	}

	public void addHistory(long d, String zip){
		fechas.add(new Date(d));
		zips.add(zip);
	}

	@Override
	public int compareTo(FileAttrib o) {
		return name.compareTo(o.name);
	}

	public String getName() {
		return name;
	}
	public long getSize(){
		return size;
	}
	public long getTime(){
		return time;
	}
	public boolean isDirectory() {
		return name.endsWith("/");
	}
	public void setFather(FileAttrib padre){
		father=padre;
		if(padre!=null)
			padre.sons.add(this);
	}
	public void setSize(long s){
		size=s;
	}
	public void setTime(long t){
		time=t;
	}
	public void showEntryHistory(boolean recurse){
		recShowEntryHistory(0,recurse);
	}

	private void recShowEntryHistory(int depth, boolean recurse){
		StringBuffer charac=new StringBuffer();
		StringBuffer blanks=new StringBuffer();
		for(int i=0;i<depth;i++){
			charac.append("|  ");
		}
		String filePart = recurse ?
				"+--" + new File(getName()).getName()
				: getName();
		blanks.append(charac);
		StringBuffer firstHistory=new StringBuffer();
		formatDate(0, firstHistory);
		System.out.print(charac);
		System.out.print(filePart+" ");
		System.out.println(firstHistory);
		while(blanks.length()<charac.length()+filePart.length()+1)
			blanks.append(" ");
		for(int i=1;i<fechas.size();i++){
			StringBuffer histor=new StringBuffer();
			histor.append(blanks);
			formatDate(i, histor);
			System.out.println(histor);
		}
		if(recurse){
			Collections.sort(sons);
			for(FileAttrib fa:sons){
				fa.recShowEntryHistory(depth+1,true);
			}
		}
	}

	private void formatDate(int i, StringBuffer histor) {
		long timemilis=fechas.get(i).getTime();
		if(timemilis<0)
			histor.append(String.format("DELETED %s", zips.get(i)));
		else
			histor.append(String.format("%1$tD %1$tR", fechas.get(i)));
	}
}
