package es.olmo.bi;

import java.util.Arrays;
import java.util.StringTokenizer;

public class List extends BackupMgr {
	String fileForList="";
	public List(String rDest, String rOri) throws Exception {
		super(rDest);
		StringTokenizer st=new StringTokenizer(rOri, "\\/");
		String pthZIPs=st.nextToken();
		init(pthZIPs);
		while(st.hasMoreElements())
			fileForList+=fileForList.length()==0?st.nextToken():"/"+st.nextToken();
	}

	public void perform(){
		String name=fileForList;
		Object sorted[]=(statDes.faList.keySet().toArray());
		Arrays.sort(sorted);
		boolean showOnlyOne=!(name.contains("*") || name.contains("?")) ;
		initMatcher(name);
		for(int i=0;i<sorted.length;i++){
			String n=sorted[i].toString();
			if(match(n)){
				statDes.faList.get(n).showEntryHistory(showOnlyOne);
				if(showOnlyOne)
					break;
			}
		}
	}
}
