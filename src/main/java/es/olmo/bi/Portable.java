package es.olmo.bi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import de.schlichtherle.truezip.zip.ZipEntry;
import de.schlichtherle.truezip.zip.ZipOutputStream;

public class Portable extends BackupMgr {
	String rd;
	static final String zipName="ExpandThisOnUSB.zip";
	
	public Portable(String rDest) throws Exception {
		super(rDest);
		rd=rDest;
	}

	public void perform() throws Exception{
		File mainDirs[]=root.listFiles();
		String fn=new File(rd).getName();
		fn+="-"+zipName;
		ZipOutputStream zf=new ZipOutputStream(new FileOutputStream(fn));
		zf.setLevel(1);
		for(File f : mainDirs){
			if(!f.isDirectory())
				continue;
			File toSave=new File(f,"STAT.gz");
			if(!toSave.exists())
				continue;
			System.out.println("Saving "+toSave.getAbsolutePath());
			String relname=toSave.getCanonicalPath().substring(root.getCanonicalPath().length()+1);
			relname=relname.replace("\\", "/");
			ZipEntry ze=new ZipEntry(relname);
			zf.putNextEntry(ze);
			flushFrom(new FileInputStream(toSave), zf, null, null);
		}
		zf.close();
		System.out.println("Saved "+fn);
	}
}
