package es.olmo.bi;


public class ProgressShower {
	
	long initTime=-1;
	String spaces="          ";
	String sameLineList=(System.getProperty("multipleListLog")!=null)?"\n":"";
	long totalSizeOfItems;
	int consoleLineSize;
	String staticMsg;
	long currentSize=0;
	
	public ProgressShower(int lineSz,int nItems, long szOfItems, String msg){
		consoleLineSize=lineSz;
		while(spaces.length()<consoleLineSize)
			spaces+=spaces;
		spaces=spaces.substring(0,consoleLineSize);
		totalSizeOfItems=szOfItems;
		staticMsg=msg;
		initTime=System.currentTimeMillis();
	}
	public ProgressShower(int lineSz,int nItems, long szOfItems, String msg, long avanceInicial){
		this(lineSz,nItems,szOfItems, msg);
		currentSize=avanceInicial;
	}
	
	public void setMessage(String m){
		staticMsg=m;
	}
	
	public void report(long avance,String msg){
		currentSize+=avance;
		System.out.print(leftMsg(msg));
	}

	public void report(int avance, String msg, String rightMsg){
		currentSize+=avance;
		String left=leftMsg(msg);
		while(rightMsg.length()>left.length())
			rightMsg=rightMsg.substring(2);
		left=left.substring(0,consoleLineSize-rightMsg.length())+rightMsg;
		System.out.print(left);
	}

	private String leftMsg(String msg) {
		// time now
		long now=System.currentTimeMillis();
		// interval transcurred til now
		long interval = now - initTime ;
		// speed of process [units per milisecond]
		float speedOfData= currentSize / (interval != 0 ? interval :1 );
		// estimate total time at current speed [miliseconds]
		float totalTimeData= totalSizeOfItems / ( speedOfData != 0 ? speedOfData : 1 ) ;
		// ETA Estimated time abend (milis)
		long eta = (long)totalTimeData - interval + 900;
		Sexagesimal stim=new Sexagesimal(eta);
		Sexagesimal elap=new Sexagesimal(interval);
		long pc=((currentSize)*100)/totalSizeOfItems;
		String show=String.format("\r%02d%% ELA %d:%02d:%02d ETA %d:%02d:%02d ", pc,
				elap.h,elap.m,elap.s,
				stim.h,stim.m,stim.s);
		show+=staticMsg+" "+msg;
		while(show.length()<consoleLineSize)
			show+=spaces;
		return show.substring(0,consoleLineSize);
	}
	
	class Sexagesimal {
		public long h,m,s;
		public Sexagesimal(long milis){
			h= milis/3600000L;
			m=(milis/1000-h*3600)/60;
			s= milis/1000-h*3600-m*60;
		}
	}
	
	public long  getCurrentValue(){
		return currentSize;
	}
}
