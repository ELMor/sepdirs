BI (Backup Incremental)
=======================	
	Is a utility to backup dirs. It's not a new thing, except for one curiosity: 

	I work wit a portable PC while traveling, and wants to be able to make a
incremental backup from a big storage on my house. Obviously I can not carry
these big storage with me, so I buid this.

	The first step is choose a dir to backup (say that, f.e. z:\Backup). I 
call it 'BackupDestination'. Then the dir i want to copy (f.e. c:\Users), for
what I exec:

	java -jar bi.jar -u -o z:\Backup c:\Users

	It creates a subdir z:\Backup\Users, then calculates all files under 
c:\Users and creates a zip with the current date there: 
	
	z:\Backup\Users\20121026-191234.zip

	And an additional one: z:\Backup\Users\STAT.gz that holds attributes of
every file zipped (date, size and name). You can maintain the same 
DestinationBackup for other backups:
 
	java -jar bi.jar -u -o z:\Backup c:\Second
	java -jar bi.jar -u -o z:\Backup c:\Third

z:\Backup holds then 
		z:\Backup\Users  (20121026-191234.zip, STAT.gz) 
		z:\Backup\Second (20121026-191341.zip, STAT.gz)
		z:\Backup\Third  (20121026-191544.zip, STAT.gz)

If I exec again other day "java -jar bi.jar -u -o z:\Backup c:\Users" another
zip is created only with new and modified files in Users dir.

 z:\Backup holds then 
		z:\Backup\Users  (20121026-191234.zip, 20121027-132320.zip, STAT.gz) 
		z:\Backup\Second (20121026-191341.zip, STAT.gz)
		z:\Backup\Third  (20121026-191544.zip, STAT.gz)

Those dirs (Users, Second, Third) are used for listing. If i wants to see if a
file is backed up (and how many times) I can do:

java -jar bi.jar -l -o z:\Backup Users\elinares\*.mp3

	I must write the first subdir, and this will list al mp3 of the zip. You
can use wildcards, but then dirs are not shown recursively. If yoy wants to 
list a dir, then you must specify it without wildcards:

	java -jar bi.jar -l -o z:\Backup Users\elinares

	Other options as 'x' extract from one of the existing zips (and uses the
same way of listing). 'r' Is used to restat dirs (I mean: re-creates the 
STAT.gz files from zips on DestinationBackup subdirs).

	One more thing: 'p' options (portable) creates a zip on the current dir 
which holds all STAT.gz of DestinationBackup, which are the unique info you 
need to create a differential backup on an USB key when traveling. When you 
return home, you must copy DestinationBackuo from key to big storage in 
house.

Generaly speaking:

java -jar bi.jar -[u|l|x|r|p]  [-d YYMMDD-HHMMSS] -o BackupDestination FileSpec

	Options can be written in a per-dir schema in a file named 
'IncBackup.properties'.

	Warning: Names are case sensitive. BI warns on console if dows not 
recognize an option.

Options can be (one per line):
	Ignore = true|false 
		If true, direcotry is skipped during backup
	UniqueVersion = true | false
		If true then all zips holding this directory are deleted after a backup is made
