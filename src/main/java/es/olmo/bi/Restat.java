package es.olmo.bi;

import java.io.File;

public class Restat extends BackupMgr {
	String rd;
	
	public Restat(String rDest) throws Exception {
		super(rDest);
		rd=rDest;
	}

	public void perform() throws Exception{
		File mainDirs[]=root.listFiles();
		for(File f : mainDirs){
			if(!f.isDirectory())
				continue;
			File toDelete=new File(f,"STAT.gz");
			System.out.println("Deleting "+toDelete.getAbsolutePath());
			toDelete.delete();
			BackupMgr bm= new BackupMgr(rd);
			bm.init(f.getName());
		}
	}

}
