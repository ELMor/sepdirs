package es.olmo.bi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import de.schlichtherle.truezip.zip.ZipEntry;
import de.schlichtherle.truezip.zip.ZipFile;


/**
 * Statistic for Backup Dir
 * @author elinares
 *
 */
public class Stat {
	public static final String deleteMark="--$$DELETED";
	File statPath=null;
	protected Hashtable<String, FileAttrib> faList=new Hashtable<>();
	String latestZip=null;
	
	public Stat(String path) throws Exception{
		statPath=new File(path).getCanonicalFile();
		if(Main.debug) 
			System.out.println("Stating source "+statPath);
		recurse(statPath.getPath());
	}
	
	private void recurse(String path) throws Exception{
		File list[]=new File(path).listFiles();
		int stripPath=statPath.getPath().length()+1;
		for(File file:list){
			String name=file.getCanonicalPath().substring(stripPath).replace('\\', '/');
			FileAttrib ze;
			if(file.isDirectory()){
				name+="/";
			}
			ze=new FileAttrib(name,
					file.isDirectory()?0:file.length(),
					file.lastModified());
			if(ze.getTime()<0){
				System.out.println("Warn: File "+file+" has modTime<0!.");
				ze.setTime(0);
			}
			faList.put(name, ze);
			if(file.isDirectory()){
				recurse(file.getCanonicalPath());
			}		
		}
	}
	
	public Stat(String parentPath, String subPathZIPs) throws Exception {
		statPath=new File(parentPath+"/"+subPathZIPs).getCanonicalFile();
		statPath.mkdirs();
		//Check is STAT file exist and load, else calculate and save
		if(!loadStatFile()){
			File list[]=statPath.listFiles();
			//Ordenar la lista
			Arrays.sort(list);
			latestZip=list[list.length-1].getAbsolutePath();
			//Procesamos los zips
			for(File file:list) {
				String name=file.getName();
				if(name.length()==19 && name.toLowerCase().endsWith(".zip")){
					if(Main.debug)
						System.out.println("Stating backup "+name);
					String dateAndTime=name.substring(0,15);
					ZipFile zf=new ZipFile(file);
					@SuppressWarnings("unchecked")
					Enumeration<ZipEntry> enuEntry=(Enumeration<ZipEntry>) zf.entries();
					while(enuEntry.hasMoreElements()){
						ZipEntry entry=enuEntry.nextElement();
						boolean deleteMode=false;
						FileAttrib ze=new FileAttrib(
								entry.getName(),
								entry.getSize(),
								entry.getTime());
						String zeName=entry.getName();
						if(zeName.endsWith(deleteMark)){
							deleteMode=true;
							zeName=zeName.substring(0,zeName.length()-deleteMark.length());
						}
						ze.addHistory(ze.getTime(),dateAndTime);
						FileAttrib ex=faList.get(zeName);
						if(deleteMode){
							FileAttrib fa=faList.get(zeName);
							if(fa!=null)
								fa.deleted=true;
						}else if(ex==null || ex.getTime()<entry.getTime()){
							faList.put(zeName, ze);
						}
					}
				}
			}
			//Find Parents...
			for(FileAttrib fa:faList.values()){
				File f=new File(fa.getName());
				String sPosPad=f.getParent();
				if(sPosPad==null)
					continue;
				sPosPad=sPosPad.replace('\\', '/')+"/";
				FileAttrib sosp=faList.get(sPosPad);
				if(sosp!=null)
					fa.setFather(sosp);
				else
					System.err.println("Cannot find parent for "+fa.getName());
			}
			saveStatFile();
		}
	}

	public void diff(Stat src, 
				Vector<FileAttrib> news, 
				Vector<FileAttrib> updated, 
				Vector<FileAttrib> untouched,
				Vector<FileAttrib> deleted) throws IOException{
		
		if(Main.debug)
			System.out.println("Diff from "+this.statPath+" to "+src.statPath);
		
		for(Enumeration<FileAttrib> zen=this.faList.elements();
				zen.hasMoreElements();)
			deleted.add(zen.nextElement());
		
		Enumeration<String> srcEnu=src.faList.keys();
		while(srcEnu.hasMoreElements()){
			String srcName=srcEnu.nextElement();
			FileAttrib thisZE=this.faList.get(srcName);
			FileAttrib srcZE = src.faList.get(srcName);
			if(thisZE==null){
				news.add(srcZE);
			}else{
				deleted.remove(thisZE);
				if(!thisZE.isDirectory() && thisZE.getTime()<srcZE.getTime()){
					updated.add(srcZE);
				}else{
					untouched.add(srcZE);
				}
			}
				
		}
	}
	
	public File getStatPath(){
		return statPath;
	}
	
	public void saveStatFile() throws Exception{
		if(Main.debug)
			System.out.print("\nSaving Stat "+statPath.getCanonicalPath());
		File statFile=new File(statPath,"STAT.gz");
		GZIPOutputStream zos=new GZIPOutputStream(new FileOutputStream(statFile));
		ObjectOutputStream oos=new ObjectOutputStream(zos);
		oos.writeObject(statPath);
		oos.writeObject(faList);
		oos.close();
		if(Main.debug)
			System.out.println(" saved");
	}
	
	@SuppressWarnings("unchecked")
	private boolean loadStatFile() throws IOException, ClassNotFoundException{
		File statFile=new File(statPath,"STAT.gz");
		if(!statFile.exists()){
			return false;
		}
		if(Main.debug)
			System.out.print("\nLoading Stat "+statPath.getCanonicalPath());
		GZIPInputStream zis=new GZIPInputStream(new FileInputStream(statFile));
		ObjectInputStream ois=new ObjectInputStream(zis);
		statPath=(File)ois.readObject();
		faList=(Hashtable<String, FileAttrib>)ois.readObject();
		ois.close();
		if(Main.debug)
			System.out.println(" loaded.");
		return true;
	}
	
}
