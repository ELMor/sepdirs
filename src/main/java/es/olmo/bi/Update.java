package es.olmo.bi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Vector;

import de.schlichtherle.truezip.file.TFile;
import de.schlichtherle.truezip.zip.ZipEntry;
import de.schlichtherle.truezip.zip.ZipOutputStream;

public class Update extends BackupMgr {

	private static String noDeflate[] = { 
		".mp3", ".ogg", ".wma",  ".zip", ".tgz",
		".tbz", ".rar", ".7z",   ".avi", ".mp4", 
		".mkv", ".wmv", ".cbr",  ".cbz", ".gz", 
		".jpg", ".png", ".jpeg", ".epub",".m4a",
		".m3a", ".apk", ".pak",  ".ipa" };
	
	Stat statOri = null;

	public Update(String rDest, String rOri) throws Exception {
		super(rDest);
		init(rOri);
		statOri = new Stat(rOri);
	}

	protected void addFiles(
			Vector<FileAttrib> zentries, 
			ZipOutputStream zip, 
			String zipName, 
			ProgressShower slp) 
			throws Exception {
		
		Collections.sort(zentries);
		for (FileAttrib zentry:zentries) {
			String fileName = zentry.getName();
			File toRead = new File(statOri.statPath, fileName);
			fileName=fileName.replace('\\', '/');
			statDes.faList.put(fileName, new FileAttrib(fileName,toRead.length(),toRead.lastModified()));
			ZipEntry ze=new ZipEntry(fileName);
			zip.putNextEntry(ze);
			if (!toRead.isDirectory()) {
				zip.setLevel(getCanCompress(fileName) ? 5 : 1);
				FileInputStream fis = new FileInputStream(toRead);
				try {
					flushFrom(fis, zip, slp, toRead.getName());
				}catch(Exception e){
					System.out.println("\nError saving "+fileName+":"+e.getMessage());
				}
				fis.close();
			}
			statDes.faList.get(fileName).addHistory(toRead.lastModified(), zipName);
		}
	}

	private void deleteFiles(
			Vector<FileAttrib> deleted, 
			ZipOutputStream newzip, 
			String zipName, 
			ProgressShower slp) 
			throws IOException {
		Collections.sort(deleted);
		for (FileAttrib ze : deleted) {
			String name = ze.getName();
			ZipEntry toAdd;
			if (ze.isDirectory()) {
				toAdd = new ZipEntry(name.substring(0, name.length() - 1)
						+ Stat.deleteMark + "/");
			} else {
				toAdd = new ZipEntry(name + Stat.deleteMark);
			}
			newzip.putNextEntry(toAdd);
			statDes.faList.get(name).addHistory(0, zipName);
			FileAttrib entr=statDes.faList.get(name);
			entr.addHistory(0, zipName);
			entr.deleted=true;
			slp.report(1, name);
		}
	}

	protected boolean getCanCompress(String fileName) {
		if (fileName.length() < 4)
			return true;
		boolean compress = true;
		String lower = fileName.toLowerCase();
		for (String ext : noDeflate) {
			if (lower.endsWith(ext)) {
				compress = false;
				break;
			}
		}
		return compress;
	}

	public String getNewZipFileName() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-kkmmss");
		String name = sdf.format(new Date());
		return name + ".zip";
	}

	public void perform() throws Exception {
		Vector<FileAttrib> news = new Vector<>();
		Vector<FileAttrib> updated = new Vector<>();
		Vector<FileAttrib> untouched = new Vector<>();
		Vector<FileAttrib> deleted = new Vector<>();
		ProgressShower slp;

		System.out.println("Updating "+statOri.statPath.getPath());
		System.out.flush();
		// BUscamos las diferencias
		statDes.diff(statOri, news, updated, untouched, deleted);
		if (news.size() == 0 && updated.size() == 0 && deleted.size() == 0) {
			System.out.println("   No changes to backup.");
			return;
		}
		String newZipName=getNewZipFileName();
		File newzip = new TFile(statDes.getStatPath(), newZipName);
		ZipOutputStream zos=new ZipOutputStream(new FileOutputStream(newzip));
		// Marcamos como borrados
		slp = new ProgressShower(80, deleted.size(), deleted.size(), "deleting ");
		deleteFiles(deleted, zos, newZipName, slp);
		// Tama�o de los ficheros a incluir en el zip
		long sz = 0;
		for (FileAttrib ze : news)
			sz += ze.isDirectory() ? 0 : ze.getSize();
		for (FileAttrib ze : updated)
			sz += ze.isDirectory() ? 0 : ze.getSize();
		slp = new ProgressShower(80, news.size()+updated.size(), sz, "adding ");
		// A�adimos los ficheros
		addFiles(news, zos, newZipName, slp);
		// Los modificados
		slp.setMessage("updating");
		addFiles(updated, zos, newZipName, slp);
		System.out.println("\nClosing zip file...");
		zos.close();
		statDes.saveStatFile();
	}
}
